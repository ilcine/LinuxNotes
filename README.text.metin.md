
## man (Manual)
Komutların açıklamalı manualını görmek için `man` komutu kullanılır.

> `man ls` ls komutunun açıklamalı manualı görülür. Görüntü sayfalarının devamını görmek için `space` tuşu, çıkmak için `q` tuşuna basılır. 

## help 

Komutların sonuna --help yazıldığında komutla ilgili kısa açıklamalar - -- ile başlayan argümanlar la listelenir. 

> * `ls --help` ls in kısa açıklaması listelenir, sayfa sayfa listeleme için  `ls --help|more` kullan.
> * `ls -l` ile dosya/dizinleri long formatta gösterir.
> * `ls -laR` # l=long ve a=all , R=alt dizinlerle birlikte göster demektir.

> Komutların sonuna `|more` yazılırsa listeleme sayfa sayfa olur. `space` tuşu ile devam edilir. 

> Komut listesini kısa yoldan görme `<tab><tab>` kullanımı; ör: l ile başlayan komutları görmek için `l<tab><tab>` kullanılır. Yazmaya başladığınız birçok komutu `<tab><tab>` tuşu ile tamamlayabilirsiniz. 

## Simge ve Açıklamalar

Simge    |Acıklama
---------|--------
`./`     | Bulunduğu dizin (veya `.`)
`../`    | Bir üst dizin 
`../../tmp` | İki üst dizindeki tmp dizini
`/`     | En üst root pozisyonu
`~`     | Kullanıcının home dizini ( öd: abc kullanısı için  `/home/abc` dir )
`~/proje1` | Kullanıcının kendi home dizininin altındaki proje1 dizini.
`-` or `--`| komutlara ilave olarak verilen özellikleri tanımlamak için  
`\`     | escape anlamındadır kendinden sonra gelen karakteri pasif olarak etkiler (ör: `'Bursa'da'` tırnak içinde tırnak kullanmak gerektiğine `\'` kullanılır)

## Dosya işlemleri

* `pwd` # bulunulan dizini gösterir
* `ls -la` # dizindeki dosyaları gösterir
* `touch  abc` # ile abc adında boş bir dosya yaratılır.
* `cat > abc` # ile abc adlı bir dosya yaratılır, sınırlı editör imkanları ile içine ifadeler yazılır “^z”  (ctrl-z) ile cat yönlenmesinden çıkılır.  

> not1: dosya yaratmak için text işleme editörleri olarak “vi, nano” veya apt ile kurarak joe gibi  başka text editörlerde kullanılır.

> not2: cat veya başka bir programın çıktısı “>” ile bir dosyaya veya (/dev/lpt1 gibi) yazıcıya yönlendirilir. Kimi durumlarda “<”  ile program içine data da konulur.

* `ls -la > dosyalarim.txt` # `ls -l` komutun çıktısı dosyalarım.txt nin içine yazılır. `cat dosyalarım.txt` ile dosya içi görülür.
* `rm dosyalarim.txt` # ile dosya silinir. 
* `rm -rf ./abc/* ` # abc dizini ve içindeki dosyalarla birlikte sil (-rf tehlikeli parametredir dikkatli kullanmak gerekir * `rm -rf /` gibi ifadeler kullanılırsa kökten başla herşeyi sil gibi bir eylemi olur)

## Dosya/Dizin yetki ve sahiplik tanımları
 
Dosya/Dizinlerin sahibi, grupları ve diğer kullanıcıların dosya/dizin üzerindeki yetkilerini tanımlar Dosyaları ve dizinleri görmek için `ls -l` kullandığımızda alttaki gibi çıktı alırız.

![ls-komutu](https://github.com/ilcine/LinuxNotlari/blob/master/png/unix.ls.png)
 
> Yetki Açıklama: ( r=read, w=write, x=eXecute anlamındadır )

* -rwxrwxrwx = 777 dosya hakkında herkez herşey yapabilir. 
* - --- ---- --- =  000 dosya hakkında kimse biryşey yapamaz.
* -rwx--- ---  = 700 sahibi herşey yapar diğerleri birşey yapamaz.
* -rw- rw- r-- = 664 sahibi ve grubu okur,yazar, diğerleri sadece okur. 

> chmod sayısının oluşumu:

`rwx in sayı değerini bulmak için sağdan sola 1,2,4 şeklinde numarala sonra onları topla; (tire işareti yetki verilmemiş anlamındadır)`

> ör1:  “rwx” için  x=1, w=2, r=4 ver, toplamı 1+2+4=7; herkez herşey yapabilir     
> ör2:  “r-x” için x=1, -=0, r=4 ver, toplamı 1+0+4=5; okuma ve çalıştırma olur.   

    Stick bitini koyma örnekleri
    chmod 1777 emr # emr dosyasına Sticky Bits koyar (t), sonuç: drwxrwxrwt
    chmod 4777 emr # emr dosyasına SUID set koyar (s), sonuç:  drwsrwxrwx
    chmod 2777 emr # emr dosyasına SGID set koyar (s), sonuç: drwxrwsrwx
    
    Stick bitini kaldırma örnekleri
    chmod u-s emr  # user stick bitini kaldırır
    chmod g-s emr  # grup stick bitini kaldırır
    chmod o-t emr  # other stikck bitini kaldırır

## Dosyaya sahiplik ve yetki verme

* `chmod 755 dosya-dizin-adi` # dosyaya 755 yetkisi verir.
* `chown emr.emr dosya-dizin-adi` # dosya sahibi emr
* `chown emr.www-data dosya-dizin-adi` # dosya sahibi emr, grubu www-data
* `chown -R emr.www-data ./public_html`  # public_html dizini ve altındakilerin sahibini değiştirir ( `-R` alt dizinlerle birlikte anlamındadır)

## Dosya özellikleri

* `file dosya.txt`   #dosya.txt in özelliklerine bakılır.
* `file --mime-encoding dosya.txt` # encding e bakılır.

Dosyalara özellik verme özellikkaldırma

* `vim +"set bomb | set fileencoding=utf-8 | wq" $(find . -type f -name ../../abc.php)`
* `vim +"set bomb | set fileencoding=utf-8 | wq" $(find . -type f -name *emr.php)`
* `vim +"set nobomb | set fileencoding=utf-8 | wq" $(find . -type f -name ./dizina/ilcin.php)`
* `iconv -f iso-8859-9 -t utf-8 tr2.html -o tr3.html`  # tr2.html yi 8859 olarak oku, tr3.html yi utf-8 olarak yaratır

## Dosya ve içerik arama

* `find ./ -name  abc`  # bulunduğum yerden başlayarak abc adlı dosyayı ara
* `find ./ -type f -exec chmod 644 {} \;` # dosya tipi f=dosya olanları bul ve 644 yetkisi ver.
* `find ./ -type f -name *.php` # dosya ek uzantısı php olanları bul.
* `find ../ -type d -name a*` # bir üst dizinden başlayarak tipi dizin olan ve adı a ile başlayan dosyaları bul.
* `find /home -printf '%s %p\n'| sort -nr | head -10` # home dizisi ve alt dizinleri içinde en büyük 10 dosyayı bul.
* `find / -type f -size +100000k -exec ls -lh {} \; | awk '{ print $9 ": " $5 }'` # en büyük dosyaları bul.
* `find . -type f -exec ls -la {} \;` # dosyaları full path olarak gösterir
* `find ./ -name "a*" -exec rm -f {} \;` # a ile başlayan dosyaları sil
* `find / -type f \( -perm -04000 -o -perm -02000 \) \-exec ls -lg {} \;` # suit biti olanları göster
* `find ./ -iname "*.php" -atime -1 -type f` #  `*.php` dosyalarının içinde hangileri 1 gün içinde değişime uğramış.
* `find ./ -cmin 20 -type f`  # 20 dakika önce değişmiş herhangi bir dosya varmı.
* `find ./ -group www-data -exec ls -lg {} \;`  # grubu www-data olanları göster.
* `find ./ -user www-data -exec ls -lg {} \;` # sahibi www-data olanları gösterir

* `du -a / | sort -n -r | head -n 10` # sistemdeki en büyük 10 dizini bulur

> ilgili tarih aralığında olanları gösterir
> * export newerthan="2012-10-04 14:01:02"
> * export olderthan="2012-10-05 15:01:02"
> * find / -newermt "$newerthan" ! -newermt "$olderthan"

* `grep “name” dosyaadi` # dosya içinde name kelimelerini göster
* `grep -R "link"  node/*`  # bulunduğum dizindeki node dizini altında bulunan tüm dosyalar içinde geçen link kelimelerini bul
* `grep "link\|yaz" dosya` # dosya içinde geçen link ve yaz kelemelerini bul

> not1: `\` ifadesi escape işaretidir, `\` den sonra gelen karakteri etkiler,  |’i komut olarak görür. 
> not2: `|` pipe işareti ile iki ifade birleştirilir. 

* `grep -rl 'Basit' ./ | xargs sed -i 's/Basit/Master/g'` // Bulunduğum dizin ve altındaki her dizin içindeki dosyaları tek tek aç içlerinde Basit kelimesini ara ve Master olarak değiştir, 

* `diff dosya1 dosya2` # dosya1 ile dosya2 yi karşılaştır farklı satırları listele
* `diff -rq vue suam2`  # dizinler arasındaki farkı bul
* `diff -rq vue suam2|grep differ |nl`  # farklı olanları sıra nosu ile göster
* `diff -rq vue suam2|grep Only |nl` # ayni olanları sıranosu ile göster.

* `head -2 abc` # abc dosyasının başındaki iki satırı göster
* `tail -10 abc.txt` # abc.txt dosyanın sonundaki 10 satarı göster
* `tail -f  /var/log/syslog` # syslog daki her haraketi sürekli göster (durdurmak için `ctrl-z`)

## Dosya da değişiklik yapma

* print edilemeyen karakterleri dosya1 den silmek için  `tr -cd '\11\12\40-\176' < dosya1 > dosya2` dosya2 yaratılır. 

## Dosya kopyalama vb

* `mv <filename1> <filename2>` # filename1 in adını filename2 yapar
* `cp <filename1> <filename2>` # filename1'i filename2 olarak çoğaltır

> scp (secure copy)
* `scp ilcin.* root@94.177.187.77:/tmp` # ilcin.* ile başlayan dosyaları uzak sunucudaki tmp dizinine kopyala(şifre sorar)
* `scp root@94.177.187.77:/tmp/emr.txt .`  # uzak suncudaki emr.txt adlı dosyayı ayni isimle getir.

> ftp (file transfer protokol)
* `ftp 94.177.187.77` # ip nolu ftp sunucusuna bağlan (login ve password sorar)
ftp komutları: `bin` binary modda dosyaları indir, `ls` uzak sunucudaki dosyaları göster, `cd dizin` ilgili dizine gir, `get dosya1` ile dosya1 i uzak sunucudan al, `put dosya2` ile dosya2 yi uzak sunucuya koy, `bye` komutu ile ftp yi sonlandır.
* Not: Sunucu anonim ise yani sadece dosya indirmeye izin veriyorsa login için `anonymous`, password için kendi e-mail adresini giriniz.

> wget # ile dosya alma
* `wget http://site.com/abc.tar` # site.com dan abc.tar adlı dosyayı getirir.

> `curl` ile sitelelerden dosya indirme yapılır. (http*, ftp, pop3, imap, ldap, scp, telnet vb birçok protokolu destekler)
* `curl http://site.com/abc.zip` # abc.zip dosyasını indirir. 
* `curl http://site.{one,two,three}.com`  # site adı ile com arasındaki sitelerden dosyaları al  
* `curl ftp://ftp.letters.com/file[a-z].txt`  # ftp sitesindeki file ile başlayan dosyaları al

> NFS (Network File System) mount komutu ile veya /etc/fstab ta tanım yapılır. 

* `mount -t nfs 94.177.187.77:/disk1/home /tmp/home` # Uzak sistemdeki disk1/home dizinini kendi sistemimizdeki /tmp/home alanına bağlamak için mount kullanılır.
* `umount /tmp/home` # ile mount işleme durdurulur

* NFS; Dizin paylaşacak 192.168.0.1 Sunucu için:
    `/etc/exports` dosyasına `/home/emr 192.168.0.2(ro,sync)` yaz; anlamı:192.168.0.2 ip nolu suncu /home/emr dizinine okuma yetkisi virilir.
    /etc/init.d/nfs start  # nfs sunucusu başlatılır.
    exportfs -ra # komutu işlemi aktif hale getirir; reboot te kullanılır; 
* NFS; Dizini Görecek 192.168.0.2 Sunucu için:
    `mount -t nfs  192.168.0.1:/home/emr`  // dizini paylaşacak sunucunun ip nosu
    `showmount -e`  # neler mount edilmiş bakılır.  
    `df` ile de bağlanılan partition görülür.

## Dizin işlemleri

* `pwd`` # bulunulan dizini gösterir
* `mkdir abc` # ile abc adlı dizin yaratılır
* `cd abc` # ile abc dizini içine girilir, 
* `cd ..`  # ile abc dizininden çıkılır. 
* `cd /tmp` # ile tmp dizinine gidilir
* `cd` # ile default dizine gelinir.
* `cd ../../etc` # iki üst dizindeki var dizinine geç
* `rmdir abc` # ile bulunduğumuz yerdeki abc dizini silinir.

## Kullanıcı işlemleri  

* `sudo adduser abc` # abc adlı kullanıcı yaratılır, `/etc/password`, `/etc/shadow`, `/etc/groups` ta kayıtlar oluşur. (root yetkisi gerekir) adduser ile kullanıcı yaratıldığında kullanıcının dizininde dosyalar oluşur . ile başlayan dosyalar kullanıcıya atanan config(.bashrc, .bash_history vb) dosyalarıdır bu dosyada yapılacak değişikliklerden sadece o kullanıcıyı etkiler. 
* `passwd abc` # abc kullanıcısının şifresi değiştirilir.
* `chfn abc`` # abc kullanıcısının özellikleri değiştirilir
* `sudo su -` # superuser moda geçilir. (prompt `#` halini alır, normal pormpt `$` dır)
* `exit` # supermod dan çıkılır,(birçok uygulamadan çıkıştıda kullanılır)
* `/etc/sudoers` # dosyası içine `abc  ALL=(ALL:ALL) ALL` verilirse abc kullanıcısı superuser olur. Duruma göre daha az yetkilerde verilir.
* `/etc/profile` # tüm kullanıcıla atanacak path vb ifadeler tanımlanır.
* `/etc/aliases` # komutlara kısayol veya kısa komut tanımı yapılır. (ör: `alias la='ls -A'` ifadesi aliases dosyasına yazılırsa `ls -la` komutu ile birlikte yeni oluşturulan `dir` komutu da kullanılır hale gelir.) aliases dosyasına yazılanların aktif olabilmesi için `newaliases` komutunun çalıştırılması gerekir. 

* .bash_history  # klavyeden girilen komutların tutulduğu dosya
* .bash_logout  # kullanıcı exit yapınca yapılacak işlemler
* .bashrc # kullanıcı login olunca yapılacak işler.
* .profile # kullanıcı ya atanan path(yol) vb ifadeler. 

* `deluser abc` #  abc adlı kullanıcıyı siler :x:

> not1: `/etc/groups` ta `abc:x:1000:` kaydı oluşur 
> not2: kullanıcıya `/home/abc` dizini açılır 

* `last` # sisteme kim ne zaman bağlanmış listelenir.
* `whoami` veya `who am i` veya `w`  # kim olduğunu söyler
* `clear` # ekranı temizler
* `quota -v`                 # shows what your disk quota is
* `date`                     # shows the current date and time
* `cal`                      # shows the month's calendar
* `wc <filename>`  # Dosyanın kaç satırdan oluştuğunu söyler
`bc -l`  # matematiksel işlemler(toplama,çıkarma,çarpma,bölme, üs alma, karakök vb) yapılır. 
`egrep -v '(#|^\s*$)'`  dosyaadi  # dosya içindeki yorum vb leri göstermez.
`unzip`, `gunzip`, `bunzip2`, `unrar`  vb  # ile sıkıştırılmış dosyalar açılır, yoksa apt ile kur.

* `tar -cvf abc.tar` abc # abc dizini abc.tar olarak tar lanır.
* `tar -xvf abc.tar` # tar ile toplanmış dizin açılır.
* `tar -tvf abc.tar` # tarlanmış dosyanın içindekiler listelenir.
* `su tar --same-owner -xvf  abc.tar` # sahip yetkileriyle açılır..


* `whereis ls`  # ls komutu nerede

* `ps -ef` # prosesleri gösterir
* `kill -9 1234`  # prosses id (PID) si 1234 olan iş sonlandırılır (ps komutu id leri gösterir)
* `killall abc` # abc adlı  işler sonlandırılır

> Uniq ve sort örnekleri
* `cat dosya1 |uniq -c` # kat tane uniq sayinin oldugunu bulur
* `sort dosya1 |uniq -c` | sort -nr |more # uniq olanları, ve kac tane olduğunu, ve numeric olarak sırala.
* `cat dosya1 | uniq -u` # Sadece bir tek kaydı olanları bulur

## Sunucu disk/cpu/memory/dosya vb durumunu görme

* `df -h` #  diskin durumunu gösterir 
* `free` # memory durumu `free -m` mb cinsinden
* `cat /proc/meminfo` # uygulamaların kullandığı memory
* `vmstat -s` # memory statusu 
* `uptime` # up kalma süresi ve avarajlar
* `top` # cpu,swap,sleeping, süreçler vb  (q=quit,1=cpu yu listeler)

## Sunucu kapama/açma

* `reboot` # sistemi kapatıp açar (shutdown komutu)
* `halt` # sistemi durdurur. (shutdown komutu)

## Konsola bağlanma
> Telnet ve ssh ile sunucu konsoluna bağlanılır.
* `telnet 94.177.187.77`  # telnet server in çalışıyor olması gerekir. login ve password sorar
* `ssh 94.177.187.77` # ssh server çalışıyor olması gerekir. login ve password sorar
> ssh config için /etc/ssh/sshd_config  # kullan
* örnekte root ve emr kullanıcıdır, group u ise adm ve root tur, public otantikasyon yapılmamıştır.

    AllowUsers root emr
    AllowGroups adm root
    PubkeyAuthentication no
    PermitRootLogin no # root ta uzaktan login olamaz.sadece konsoldan bağlanır.

## PATH dizinlere yol verilmesi (kısayol) 

* `echo $PATH` # ile mevcut path’ler gösterilir  
* `export PATH="$PATH:/usr/local/bin`  # PATH a yeni PATH verilir. örnekte `/usr/local/bin` içindeki komutlar path(yol) gösterilmeden çalıştırılır. (komut olarak çalıştırılırsa login olunduğu sürece path aktif olur)

* `~/.bashrc`  # path ifadesi buraya yazılırsa o kullanıcı için sürekli geçerli olur. 
* `/etc/profile` # path ifadesi buraya yazılırsa herkez için sürekli geçerli olur.

> * not1: `~/` tilde ifadesi kullanıcının home dizinini ifade eder ör:/home/abc, `~/.bashrc` ile `/home/abc/.bashrc` ayni şeydir.
> * not2: profile ye `export HISTSIZE=10000` yazılırsa klavyeden girilmiş son 10000 komut `.bash_histroy` dosyasında saklanır (güvenlik için önemli), komutun aktif olması için logout ve login gerekir veya `source .profile` komutu ile HISTSIZE aktif hale getirilir.
> * not3: komut yazılırken bir boşluk verilirse ~.bash_history~ e birşey yazılmaz.
> * not4: profile ye yazılan `umask 022` ile yaratılacak dosya ve dizinlerin default yetkileri (644) tanımlanır. `(because 0666 & ~022 = 0644; i.e., rw-r--r--)` , erişim yetkilerinin neler olduğu `acl` (access control list) te tanımlanmıştır. 

## Network işlemleri

* `netstat -a` # listen,estableshed, giren çıkan paketler port noları listelenir.
* `netstat -tunap` # tcp/udp portların uygulama isimleriyle listelenir.
* `route` # yönlenmeleri gösterir
* `hostname` # hostun adını gösterir
* `hostname -d` # domain i gösterir.
* `hostname www.ilcin.name.tr` # hostu ve domain i değiştirir 
* `hostname -a`  # verilmiş alias adları gösterir
* `cat /etc/resolv.conf`   #dns çözümlemesi yapan hostları gösterir
* `cat /etc/network/interfaces`  #  atanmış ip, mask, dns leri gösterir
* `ifconfig`  #atanmış ethernetleri gösterir
* `ifconfigi eth0:1  192.168.0.2`  # eth0 a ikinci bir ip daha ver; ( bir ethernete birçok ip atanabilir)   
* `ping www.google.com`  # google e erişiliyormu sinyali gönderir.
* 'fping' # alternatif ping apt ile kur; tüm network class lardaki ip lerin, alive(canlı) olup olmadığı vb görür
* `dig <domain>`  # gets DNS information for domain
* `dig -x <host>`  # reverses lookup host varmı
* `dig uludag.edu.tr a +short` # uludag a dns kaydı sorgusu
* `nslookup -q=a www.ilcin.name.tr`  # default dns e sor, host'un A kaydı varmı sor
* `nslookup -q=any ilcin.name.tr 8.8.8.8`  # ilcin.name.tr domain'i, google dns te tanımlımı
* `nslookup -q=any uludag.edu.tr 195.175.39.49` # ttnet dns te kaydı varmı (tr den sor)
* `ip route show` // ipv4 durumu
* `netstat -nr -6`  // ipv6 durumu
* `ip -6 route show` // ipv6 route durumu
* `nmap -v -A localhost` // port ları tarar.
* `nmap -sS -sU -PN  localhost` # sunucumuz syn,ack gibi connectionları ve  port kullanımlarını gösterir
* `nmap -sS google.com.tr`  # google sunucusundaki açık portları gösterir.

## Editörler hakkında kısa kısa

* `vi` # Tüm linux ve unix lerin standart editörüdür.
* `vi dosya.txt  # ile dosya açılır. 
> iki modu vardır. 
* mod1:  (i) `insert` veya (a) `append` tuşu ile yazma moduna geçilir.
* mod2:  `[esc]:` (escape tuşu ile birlikte :) ile komut moduna geçilir prompt `:` olur
* `[esc]q!` # ile çıkış yapılır.
* `[esc]wq`  # içeriği save (sakla) ve çıkış yap

* `nano dosya.txt` # ile dosya açılır nano penceresi altında yapılacak işlemleri gösteren help (yardım) tuş takımları vardır.`[ctrl]x` # ile çıkış yapılırken yapılmış değişiklikler varsa y/n ile save (saklama) yapılır.

* `sudo update-alternatives --config editor` # ile default etötür danımlanır.
* `export EDITOR=nano` # ile o an için 
* `set` # komutu path,alias,editor vb gibi daha önce yapılmış tanımları gösterir.

## SED Komutu

* `sed -i  'g/^$/d' dosya.txt`  # dosyadaki boş satırları siler 
* `sed “s/^ *//”  dosya`   # satirin sol tarafini siler
* `sed “s/ *$//”  dosya`   # satirin sag tarafindaki bosluklari siler
* `echo "aaa:bursa:bbb"| sed 's/\:[a-z]*\://g'`  # bursa yı siler
* `sed "s/$/\"/g"  emr.ls.own.txt > emr.ls.own.s1.txt`  #satir sonuna " koyar
* `sed "s/ \/home/ \"\/home/g"  emr.ls.own.s1.txt > emr.ls.own.s2.txt` # /home nin basina "/home koyar
* `echo "ab ab       ab"| sed 's/  */ /g'` # birden fazla boslugu teke dusurmek
> not1: `-i` dosyaya yazar  

> not2: `<` ile sol `>` ile sağ taraf etkilenir.

[sed](http://sed.sourceforge.net/sed1line.txt)

## AWK Komutu

[awk](https://www.geeksforgeeks.org/awk-command-unixlinux-examples/)

## CRONTAB (zamanlanmış görevler)

* `sudo update-alternatives --config editor` # kullanılacak editörü seç 
* `crontab -l` # yazılanlar listelenir.
* `crontab -e` # ile editleme yap

```
.---------------- dakika (0 - 59)
|  .------------- saat (0 - 23)
|  |  .---------- Ayın Günleri (1 - 31)
|  |  |  .------- Ay (1 - 12)
|  |  |  |  .---- Haftanın Günleri (0 - 6) (Pazar=0)
|  |  |  |  |
*  *  *  *  *  Çalıştırılacak komut
```

* (*) ile işin her gün,her ay, her saat vb çalışması istenir 
Örnekler
* `* 17 * * * sudo /home/emr/run.sh` : saat 17 yi gösterdiğinde run.sh script kodu çalışsın. `
* `*/5 * * * * sudo /home/emr/run.sh` : her 5 dk bir run.sh script kodu çalışsın. (/) ile kademeler belirlenir.
* `* */5 * * * sudo /home/emr/run.sh` : her 5 saate bir run.sh çalışsın.
* `* 1-15 * * * sudo /home/emr/run.sh` : Her ayın 1 inde ve 15 inde run.sh çalışsın. (-) ile zaman aralığı belirlenir.
* `0 0 1 1 * sudo /home/emr/run.sh` : Yeni yılın ilk günü saat 0 0 da çalışsın. 
* `0 9-17 * * * sudo /home/emr/run.sh` : Her gün saat 9 dan 17 ye kadar çalışsın sonra dursun. 
* `0 12 * 2 1,5 sudo /home/emr/run.sh` : Şubat ayının her pazartesi ve cuma günü saat 12 de çalışsın. (,) ile birden fazla değer girilir.

## LOGROTATE ( Logların ne zaman alınacağı ve ne kadar tutulacağını belirler ) 

* `/etc/logrotate.conf  # default config ler tanımlanır.
* `/etc/logrotate.d/vsftpd` # Örnek: `vsftpd` ftp logları haftalık dosyalarda, bir ay olarak tutulacak( compress, start stop gibi ilave özelliklerde yazılır)

```
/var/log/vsftpd.log
 {
        create 640 root adm
        missingok
        notifempty
        rotate 4
        weekly
}
```

## QUOTA ( kullanıcılara disk kotası dağıtır)

> apt install quoata ile kurulur; `/etc/fstab` dosyasına tanım yapılır, 

* ör:/home dizinine kota verilecekse fstab dosyasında `/home defaults,usrquota,noexec` yetki veya yetkiler verilir. veya tüm sisteme kota tanımlanır. Ör: 
* `/dev/mapper/vg_server1-lv_root / ext4 defaults,usrjquota=aquota.user,grpjquota=aquota.group,jqfmt=vfsv0  1 1`
* `quotacheck -cug /home`   # bir kere yapılır db yaratir 
* `quotacheck -avugm` # kotaları check eder.
* `quotaon -avug` # kotayı aktif hele getirir.
* `quotaoff -avug` # kota işlemi durur.
* `setquota -a emr 10000 10000 0 0` # emr kullanıcısına 10mb kota tanımlar
* `repquota -a` # kota hakkında rapor verir

> not: kota /home ye atandı ise home dizininde `quota.user` dosyası oluşur.

## Saatın ayarlanması

> linux larda ntp ile internet üzerinden saat güncellemesi yapılır. debian da ntp serverler tanımlıdır. hostlar `/etc/ntp.conf` da tanımlıdır.

* `ntpq -p` # ntp server ler listelenir.
* `date -R` # günün gösterilmesi 
* `date +"%d-%m-%Y"`  # gün/ay/yıl formatında gösterir
* `date '+%Y-%m-%d %H:%M:%S'` # tarih ve saati gösterir (`date --help` # detayı gösterir)
* `hwclock`   #Donanım saatini okur.                                   
* `hwclock --systohc --utc`  # Donanım saatini İşletim Sisteminin geçerli zamanını baz alarak UTC'ye ayarlar.                        
* `hwclock --systohc`   # Donanım saatini İşletim Sisteminin geçerli zamanını baz alarak yerel zamana ayarlar.                           
* `hwclock --set --date "21 Oct 2004 21:17"`  # Donanım saatini dizgedeki zamana ayarlar.  
* `setclock`  # hwclock un gelişmiş halididir 

```
    Kurulum için: 
    apt install ntp
    dpkg-reconfigure ntp
    systemctl enable ntp.service  # ile açılışta çalışması söylenir.
```

## Güvenlik işlemleri 
> Sisteme bulaşmış rootkit leri bulma.
> apt install chkrootkit ve apt install rkhunter # kurulur.
* `chkrootkit` # ile rootkit taraması yapılır
* `rkhunter --check` # ile rootkit taraması yapılır ( --update ile güncelleme yapılır)

> sisteme bulaşmış virusleri bulma.
* `apt install clamav clamav-daemon` # ile kurulum yap
*`clamscan file­­` bir dosyayı tara
* `clamscan --recursive=yes --infected /home` # home dizinini tara
* `clamscan --max-filesize=2000M --max-scansize=2000M --recursive=yes --infected /home` # dosya sayısı 20mb den büyükse 

## Diger komutlar
* `rev` # ters çevirir  ­­`echo "emr" | rev` # sonuc rme
* `tac` # cat ın tersi # dosyaların başı sona gelir.
* 'tee' # standart çıktıyı yönlendirir. 

##BASH (Kabuk işlemleri)

[bash1](http://homepage.smc.edu/morgan_david/cs41/bash.txt)
[bash2](https://gist.github.com/LeCoupa/122b12050f5fb267e75f)
[bash3](https://ss64.com/bash/)
[shell-1](http://archive.is/FRjCE)

***Devam edecek***

