## Dizin işlemleri

* `pwd` # Bulunulan dizini gösterir
* `mkdir abc` # İle abc adlı dizin yaratılır
* `cd abc` # İle abc dizini içine girilir, 
* `cd ..`  # İle abc dizininden çıkılır. 
* `cd /tmp` # İle tmp dizinine gidilir
* `cd` # İle default dizine gelinir.
* `cd ../../etc` # İki üst dizindeki var dizinine geç
* `rmdir abc` # İle bulunduğumuz yerdeki abc dizini silinir.
