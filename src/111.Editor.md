## Editörler hakkında 

Metinleri düzenlemek için birçok editör kullanılır; 

`sudo update-alternatives --config editor` # ile default editör tanımlanır;
`export EDITOR=nano` # ile o an için 

###  `vi dosya.txt`

_Tüm linux ve unix lerin standart editörüdür._

>İki modu vardır; mod1:  (i) `insert` veya (a) `append` tuşu ile yazma moduna geçilir;
mod2:  `[esc]:` (escape tuşu ile birlikte :) ile komut yazma moduna geçilir prompt `:` olur

* `[esc]q!` # ile çıkış yapılır.
* `[esc]wq`  # içeriği save (sakla) ve çıkış yap

### `nano dosya.txt` 

*  `^X` : `[ctrl]` tuşu ve X ile çıkış yapar; değişiklikler varsa y/n ile save (saklama) yapar.
*  `^O` : saklama;

### `joe dosya.txt` 

* `^KH`: `[ctrl]`tuşu ile birlikte K ve H; Help menüsüdür.
* `^KD`: saklama; 
* `^KQ`: çıkış;


 