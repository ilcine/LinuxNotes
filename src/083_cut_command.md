
# cut

## choose

```
echo "a,b,c,d,e"| cut -d',' -f2,3    // export b,c
```

## exclude
```
echo "a,b,c,d,e"| cut --complement -d',' -f2,3    // export a,d,e
```
